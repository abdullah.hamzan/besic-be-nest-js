import {
  ArgumentMetadata,
  Injectable,
  PipeTransform,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { CreateUserDto } from 'src/users/controller/dtos/CreateUserDto';

@Injectable()
export class ValidateCreateUserPipe implements PipeTransform {
  transform(value: CreateUserDto, metadata: ArgumentMetadata) {
    console.log('inside validateCreateuser');

    console.log(value);
    console.log('metadata', metadata);
    const parseIntAge = parseInt(value.age.toString());

    if (isNaN(parseIntAge)) {
      console.log('resultParseIntAge:', parseIntAge);
      throw new HttpException('Invalid send age', HttpStatus.BAD_REQUEST);
    }

    return { ...value, age: parseIntAge };
  }
}
