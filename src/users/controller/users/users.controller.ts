import {
  Controller,
  Get,
  Post,
  Req,
  Res,
  Body,
  Param,
  Query,
  ValidationPipe,
  UsePipes,
  ParseIntPipe,
  HttpException,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { AuthGuard } from 'src/users/guards/auth/auth.guard';
import { ValidateCreateUserPipe } from 'src/users/pipes/validate-create-user/validate-create-user.pipe';
import { UsersService } from 'src/users/services/users/users.service';
import { CreateUserDto } from '../dtos/CreateUserDto';

@Controller('users')
export class UsersController {
  constructor(private userService: UsersService) {}

  @Get()
  @UseGuards(AuthGuard)
  getUsers(@Query('sortBy') sortBy: string) {
    console.log(sortBy);
    return this.userService.fetchUser();
  }

  @Get('posts')
  getPosts() {
    return [
      {
        username: 'Abdul',
        email: 'Abdul Hamzan',
        posts: [
          {
            id: 1,
            title: 'Hello',
            content: 'Hello broo',
          },
          {
            id: 2,
            title: 'Hayii',
            content: 'Hayii broo',
          },
        ],
      },
    ];
  }

  @Post()
  createUser(@Req() request: Request, @Res() response: Response) {
    console.log(request.body);

    return response.send('Created Succes');
  }

  @Post('create')
  @UsePipes(new ValidationPipe())
  createAccount(@Body(ValidateCreateUserPipe) userData: CreateUserDto): any {
    console.log(userData.age);

    return this.userService.createUser(userData);
  }
  @Get(':id')
  getUserByID(@Param('id', ParseIntPipe) id: number) {
    console.log(id);
    const user = this.userService.fethUserById(id);
    if (!user) {
      throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
    }

    return user;
  }
}
