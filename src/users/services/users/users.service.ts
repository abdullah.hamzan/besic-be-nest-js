import { Injectable } from '@nestjs/common';
import { CreateUser } from 'src/utils/type';

@Injectable()
export class UsersService {
  private users = [
    { name: 'abdullah', email: 'abdullah.hamzan@gmail.com' },
    { name: 'Malik', email: 'malik@gmail.com' },
  ];

  fetchUser() {
    return this.users;
  }

  createUser(userDetail: CreateUser) {
    this.users.push(userDetail);
    return;
  }

  fethUserById(id: number) {
    return { name: 'abdul', email: 'abdullah.hamzan@gmail.com' };
  }
}
